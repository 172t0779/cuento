package com.limontellezalejandro.cuento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_enriqueta_p5.*

class EnriquetaP5 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enriqueta_p5)
        siguiente.setOnClickListener {
            val intent= Intent(this,EnriquetaP6::class.java)
            startActivity(intent)
        }
    }
}