package com.limontellezalejandro.cuento


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_enriqueta_p8.*


class EnriquetaP8 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enriqueta_p8)
        dale.setOnClickListener{
            val intent= Intent(this,Enriqueta4::class.java)
            startActivity(intent)
        }
    }

}