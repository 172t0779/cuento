package com.limontellezalejandro.cuento
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_menu.*

class Menu : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        val bundle: Bundle? = intent.extras
        bundle?.let { bundleLibriDeNull ->
            val nombres = bundleLibriDeNull.getString("key_nombre", "")
            val sexo = bundleLibriDeNull.getString("key_sexo", "")
            usario.text = nombres
            if (sexo == "F") {
                icono.setImageResource(R.drawable.user2)
            } else if (sexo == "M") {
                icono.setImageResource(R.drawable.user1)
            } else if (sexo == "m") {
                icono.setImageResource(R.drawable.user1)
            } else if (sexo == "f") {
                icono.setImageResource(R.drawable.user2)
            }
        }

        boton00.setOnClickListener {
            val intent = Intent(this, EnriquetaP1::class.java)
            startActivity(intent)
        }
        boton01.setOnClickListener {
            Toast.makeText(
                this,
                "Los paragüitas aventureros no esta disponibles",
                Toast.LENGTH_SHORT
            ).show()
        }
        boton02.setOnClickListener {
            Toast.makeText(
                this,
                "Los juguetes de Guille se aburren no esta disponible",
                Toast.LENGTH_SHORT
            ).show()
        }
        boton03.setOnClickListener {
            Toast.makeText(
                this,
                "El papagayo peluquero no esta disponible",
                Toast.LENGTH_SHORT
            ).show()
        }
        boton04.setOnClickListener {
            Toast.makeText(
                this,
                "La excursión del dinosaurio sandi no esta disponible",
                Toast.LENGTH_SHORT
            ).show()
        }
        boton05.setOnClickListener {
            Toast.makeText(
                this,
                "zulema, la ciudad Sucia no esta disponible",
                Toast.LENGTH_SHORT
            ).show()
        }
        boton06.setOnClickListener {
            Toast.makeText(
                this,
                "Las vacaciones del pavo walter no esta disponible",
                Toast.LENGTH_SHORT
            ).show()
        }
        boton07.setOnClickListener {
            Toast.makeText(
                this,
                "Merry, la mariposa mentirosa no estan disponibles",
                Toast.LENGTH_SHORT
            ).show()
        }
        boton08.setOnClickListener {
            Toast.makeText(
                this,
                "Brote, el duende con dos dientes no estan disponibles",
                Toast.LENGTH_SHORT
            ).show()
        }
        boton09.setOnClickListener {
            Toast.makeText(
                this,
                "kiko, el caballo que solo comia queso no estan disponibles",
                Toast.LENGTH_SHORT
            ).show()
        }
        boton10.setOnClickListener {
            Toast.makeText(
                this,
                "Marci y Ciano dos niños marcianos no estan disponibles",
                Toast.LENGTH_SHORT
            ).show()
        }


    }
}