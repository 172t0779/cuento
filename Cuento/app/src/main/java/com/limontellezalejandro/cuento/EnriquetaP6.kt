package com.limontellezalejandro.cuento
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import kotlinx.android.synthetic.main.activity_enriqueta_p6.siguiente


class EnriquetaP6 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enriqueta_p6)
        siguiente.setOnClickListener {
            val intent= Intent(this,EnriquetaP7::class.java)
            startActivity(intent)
        }
    }
}