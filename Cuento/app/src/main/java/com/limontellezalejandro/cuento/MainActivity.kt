package com.limontellezalejandro.cuento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn.setOnClickListener {
            val nombres:String=us.text.toString()
            if(nombres.isEmpty()){
                Toast.makeText(this, "Ingrese un nombre", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val sexo:String=sx.text.toString()
            if(sexo.isEmpty()){
                Toast.makeText(this, "Ingrese su sexo", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val bundle=Bundle()
            bundle.apply {
                putString("key_nombre",nombres)
                putString("key_sexo",sexo)
            }
            val intent= Intent(this, Menu::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }

    }
}