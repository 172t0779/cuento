package com.limontellezalejandro.cuento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_enriqueta4.*

class Enriqueta4 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enriqueta4)
        siguiente.setOnClickListener {
            val intent= Intent(this,EnriquetaP5::class.java)
            startActivity(intent)
        }
    }
}